byte red   = 6;  // Красные светодиоды на 5 пине.
byte green = 10;  // Зеленые светодиоды на 6 пине.
byte blue  = 3;  // Синие светодиоды на 3 пине.

// Номер текущего эффекта.

int i;

int byt;
int stb;

int r = 0;
int g = 0;
int b = 0;

static int state = 0;
static int index = 0;

void setup()
{
  // Т.к. мы используем ШИМ, то настраивать порты для ленты не надо.
  Serial.begin(9600);
}

void loop()
{

  if (Serial.available()) {
    byt = Serial.read();
  }
  switch (byt)
  {
    case '0': //Выключение всех светодиодов.
      analogWrite(red,   0);
      analogWrite(green, 0);
      analogWrite(blue,  0);
      break;

    case '1': // Эффект №1: Включение всех светодиодов.
      analogWrite(red, 255);
      analogWrite(green, 255);
      analogWrite(blue, 255);
      break;

    case '2': // Эффект №2: Бегущий огонек.
      if (state == 3)
      {
        analogWrite(red,   255);
        analogWrite(green, 0);
      }
      if (state == 6)
      {
        analogWrite(blue, 255);
        analogWrite(red,  0);
      }
      if (state == 9)
      {
        analogWrite(green, 255);
        analogWrite(blue,  0);
      }
      state--;
      if (state < 1) state = 9;
      break;

    case '3': // Эффект №3: Бегущий огонек (в обратную сторону).

      if (state == 1)
      {
        analogWrite(blue,  255);
        analogWrite(green, 0);
      }\
      if (state == 4)
      {
        analogWrite(green, 255);
        analogWrite(red,   0);
      }
      if (state == 7)
      {
        analogWrite(red,  255);
        analogWrite(blue, 0);
      }
      state++;
      if (state > 9) state = 1;
      break;

    case '4': // Эффект №4: Плавное мигание
      bright();
      blind();
      break;

    case '5': // Рандомное мигание.
      analogWrite(red,   random(0, 255));
      analogWrite(green, random(0, 255));
      analogWrite(blue,  random(0, 255));
      break;

    case '6': //Orange.
     analogWrite(red, 255);
     analogWrite(green, 255);
     analogWrite(blue, 0);
     delay(2000);
     break;

    case '7': //Pink
     analogWrite(red, 255);
     analogWrite(blue, 255);
     analogWrite(green, 0);
     delay(2000);
     break;

    case '8': //Cyan
     analogWrite(green, 255);
     analogWrite(blue, 255);
     analogWrite(red, 0);
     delay(2000);
     break;

    case '9': //Переключение между 6, 7 и 8.
     unlight();
     break;

    case 'A':
     r = Serial.read();
     analogWrite(red, r);
     delay(2000);
     break;

    case 'B':
     g = Serial.read();
     analogWrite(green, g);
     delay(2000);
     break;

    case 'C':
    b = Serial.read();
    analogWrite(blue, b);
    delay(2000);
    break;
  }

  // Задержка в 50мс.
  delay(50);
}


void bright()
{
  analogWrite(red, 0);
  analogWrite(green, 0);
  analogWrite(blue, 0);
  delay(150);

  analogWrite(red, 3);
  analogWrite(green, 3);
  analogWrite(blue, 3);
  delay(150);

  analogWrite(red, 7);
  analogWrite(green, 7);
  analogWrite(blue, 7);
  delay(150);

  analogWrite(red, 15);
  analogWrite(green, 15);
  analogWrite(blue, 15);
  delay(150);

  analogWrite(red, 31);
  analogWrite(green, 31);
  analogWrite(blue, 31);
  delay(150);

  analogWrite(red, 63);
  analogWrite(green, 63);
  analogWrite(blue, 63);
  delay(150);

  analogWrite(red, 95);
  analogWrite(green, 95);
  analogWrite(blue, 95);
  delay(150);

  analogWrite(red, 127);
  analogWrite(green, 127);
  analogWrite(blue, 127);
  delay(150);

  analogWrite(red, 159);
  analogWrite(green, 159);
  analogWrite(blue, 159);
  delay(150);

  analogWrite(red, 191);
  analogWrite(green, 191);
  analogWrite(blue, 191);
  delay(150);

  analogWrite(red, 223);
  analogWrite(green, 223);
  analogWrite(blue, 223);
  delay(150);

  analogWrite(red, 255);
  analogWrite(green, 255);
  analogWrite(blue, 255);
  delay(550);

}



void blind()
{
  analogWrite(red, 255);
  analogWrite(green, 255);
  analogWrite(blue, 255);
  delay(150);

  analogWrite(red, 223);
  analogWrite(green, 223);
  analogWrite(blue, 223);
  delay(150);

  analogWrite(red, 191);
  analogWrite(green, 191);
  analogWrite(blue, 191);
  delay(150);

  analogWrite(red, 159);
  analogWrite(green, 159);
  analogWrite(blue, 159);
  delay(150);

  analogWrite(red, 127);
  analogWrite(green, 127);
  analogWrite(blue, 127);
  delay(150);

  analogWrite(red, 95);
  analogWrite(green, 95);
  analogWrite(blue, 95);
  delay(150);

  analogWrite(red, 63);
  analogWrite(green, 63);
  analogWrite(blue, 63);
  delay(150);

  analogWrite(red, 31);
  analogWrite(green, 31);
  analogWrite(blue, 31);
  delay(150);

  analogWrite(red, 15);
  analogWrite(green, 15);
  analogWrite(blue, 15);
  delay(150);

  analogWrite(red, 7);
  analogWrite(green, 7);
  analogWrite(blue, 7);
  delay(150);

  analogWrite(red, 3);
  analogWrite(green, 3);
  analogWrite(blue, 3);
  delay(150);

  analogWrite(red, 0);
  analogWrite(green, 0);
  analogWrite(blue, 0);
  delay(550);


}

void unlight()
{ 
  analogWrite(red, 255);
  analogWrite(green, 255);
  delay(2000);

  analogWrite(red, 0);
  analogWrite(green, 0);

  analogWrite(red, 255);
  analogWrite(blue, 255);
  delay(2000);

  analogWrite(red, 0);
  analogWrite(blue, 0);
  

  analogWrite(green, 255);
  analogWrite(blue, 255);
  delay(2000);

  analogWrite(green, 0);
  analogWrite(blue, 0);
}


