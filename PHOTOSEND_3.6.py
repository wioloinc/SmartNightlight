import telebot
import serial
import time
import config
from telebot import apihelper
import time
from telebot.util import async
import requests
import uuid
from PIL import Image



token = '676669654:AAGou4_BP2dnn8neWxFPcu4O6hEpa7SurPE'

#Переменная для хранения среднего значения цвета
sumr = 0
sumg = 0
sumb = 0

#Создаем обьект для серийного порта
ser = serial.Serial('COM6', 9600, timeout=1)

#перед началом работы надо хорошенько выспаться
time.sleep(1)

#Создаем обьект бота
bot = telebot.TeleBot(token=token)

#Используем прокси (скопируйте эту строку целиком)
apihelper.proxy = {'https': 'socks5://goto:gotogotogoto@tvorog.me:6666'}

#Принятие текста.
@bot.message_handler(content_types=["text"])
@async()
def repeat_all_messages(message):
    if message.text == '/start':
        tex = 'Доступные команды: '
        bot.send_message(message.chat.id, tex)
        tex = '1 - Вкл.'
        bot.send_message(message.chat.id, tex)
        tex = '2 - Бегущая линия.'
        bot.send_message(message.chat.id, tex)
        tex = '3 - Бегущая линия (в обратную сторону)'
        bot.send_message(message.chat.id, tex)
        tex = '4 - Плавное освещение.'
        bot.send_message(message.chat.id, tex)
        tex = '5 - Хаотичное мигание.'
        bot.send_message(message.chat.id, tex)
        tex = '6 - Оранжевый, розовый и бирюзовый.'
        bot.send_message(message.chat.id, tex)
        tex = '7 - Розовый.'
        bot.send_message(message.chat.id, tex)
        tex = '8 - Бирюзовый.'
        bot.send_message(message.chat.id, tex)
        tex = '9 - Переключение между orange, pink & cyan.'
        bot.send_message(message.chat.id, tex)
        tex = 'Режим подборки оптимального освещения (beta) - сделать фотографию.'
        bot.send_message(message.chat.id, tex)
        
    #Если прислали единичку
    if message.text == '1':
        #Отправляем что-то в серийный порт
        ser.write(b'1')
    if message.text == '0':
        ser.write(b'0')
    if message.text == '2':
        ser.write(b'2')
    if message.text == '3':
        ser.write(b'3')
    if message.text == '4':
        ser.write(b'4')
    if message.text == '5':
        ser.write(b'5')
    if message.text == '6':
        ser.write(b'6')
    if message.text == '7':
        ser.write(b'7')
    if message.text == '8':
        ser.write(b'8')
    if message.text == '9':
        ser.write(b'9')
        #Отчитываемся о проделанной работе
    tex = 'Выполнена команда '+ message.text
    bot.send_message(message.chat.id, tex)

def process(filename):
    tex = ('Что-то происходит')
    bot.send_message(message.chat.id, tex)
    print('Процесс идет')
    img = Image.open(filename)
    box = (0, 0, img.width // 2, img.height)
    left = img.crop(box)
    left = left.transpose(Image.FLIP_LEFT_RIGHT)
    img.paste(left, (img.width // 2, 0))
    img.save(filename)

@bot.message_handler(content_types=['photo'])
def photo(message):
    global sumr
    global sumg
    global sumb
    # скачивание файла
    print('Скачивание файла')
    tex = ('Скачиваю фотографию..')
    bot.send_message(message.chat.id, tex)
    file_id = message.photo[-1].file_id
    path = bot.get_file(file_id)
    downloaded_file = bot.download_file(path.file_path)
    tex = ('Сохранение')
    bot.send_message(message.chat.id, tex)
    file_id = message.photo[-1].file_id
    path = bot.get_file(file_id)
    extn = '.' + str(path.file_path).split('.')[-1]
    downloaded_file = bot.download_file(path.file_path)
    cname = str(uuid.uuid4()) + extn
    tex = ('/-*/wait cpp error ыы4 not connect 1 sec/*-/')
    bot.send_message(message.chat.id, tex)
    with open('images/' + cname, 'wb') as new_file:
        new_file.write(downloaded_file)
    tex = ('Обработка фотографии..')
    bot.send_message(message.chat.id, tex)
    im = Image.open('images/' + cname, mode='r')
    pixels=im.load()
    x, y = im.size
    ser.write(b'A')
    for i in range(im.width):
        print('Начинаем внешний цикл')
    for j in range(im.height):
            print('Начинаем внутренний цикл')
            r, g, b = pixels[i, j]
            sumr = sumr + r
            sumg = sumg + g
            sumb = sumb + b
    tex = ('25% - считывание пикселей')
    bot.send_message(message.chat.id, tex)
    tex = ('50% - расчёт полученных данных')
    bot.send_message(message.chat.id, tex)
    ra = 2 * sumr//(im.width * im.height)
    ga = 2 * sumg//(im.width * im.height)
    ba = 2 * sumb//(im.width * im.height)
    tex = ('75% - прием данных микроконтроллером')
    bot.send_message(message.chat.id, tex)
    ser.write(b'*')
   
    ser.write(r)
    ser.write(b' ')
    print(r)
    
            
    ser.write(g)
    ser.write(b' ')
    print(g)
    
   
    ser.write(b)
    ser.write(b' ') 
    print(b)
    

    tex = ('Готово! :)')
    bot.send_message(message.chat.id, tex)
   
#Принятие изображения.
#@bot.message_handler(content_types=["photo"])
#@async()
#def save(message):

                 
bot.polling(none_stop=True)
